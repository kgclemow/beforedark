﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DontDestroy : MonoBehaviour
{
    public GameObject loseTextObject;

    public AudioSource music;
  
    public TextMeshProUGUI Timer;

    public static float timeLeft = 160.0f;

    private static DontDestroy instance = null;

    public static DontDestroy Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }


        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }


    void Start()
    {
        loseTextObject.SetActive(false);
    }


    public void RestartTimer()
    {
        timeLeft = 160.0f;
    }

    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            Timer.text = timeLeft.ToString();
        }
        else
        {
            EndGame();
        }
    }

    void EndGame()
    {
        loseTextObject.SetActive(true);

        timeLeft = 160.0f;
    }
}
