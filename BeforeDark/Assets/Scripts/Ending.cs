﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Ending : MonoBehaviour
{
    public GameObject loseTextObject;
    public GameObject winTextObject;

    public TextMeshProUGUI Timer;

    private float timeLeft = 170.0f;

    void Start()
    {
        loseTextObject.SetActive(false);
        winTextObject.SetActive(false);
    }

    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            Timer.text = timeLeft.ToString();
        }
        else
        {
            EndGame();
            Destroy(Timer);
        }
    }

    void EndGame()
    {
        loseTextObject.SetActive(true);
    }
}
